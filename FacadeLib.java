package edu.ucsd.cse110.library;

/*
 * HOMEWORK FILE
 */
public class FacadeLib {

  // Mediator decouples interaction between Members and Publications
  Mediator med;

  public FacadeLib(IMediator m){
    this.med = med;
  }

  public static void checkoutPublication(Member m, Publication p){

    this.med.checkoutPublication(m, p);

  }

  public static void returnPublication(Publication p){

    this.med.returnPublication(p);

  }

  public static double getFee(Member m){

    return this.med.getMemberFees(m);

  }

  public static boolean hasFee(Member m){

    return this.med.checkMemberFees(m);

  }


}
