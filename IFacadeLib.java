package edu.ucsd.cse110.library;

/*
 * HOMEWORK FILE
 */
public class IFacadeLib {

  public void checkoutPublication(Member, Publication);

  public void returnPublication(Publication);

  public double getFee(Member);

  public boolean hasFee(Member);


}
