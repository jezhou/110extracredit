package edu.ucsd.cse110.library;

import java.time.LocalDate;

/*
 * HOMEWORK FILE
 */
public class MediatorLib implements IMediator{

  public void checkoutPublication(Member m, Publication p){

    if(!p.isCheckOut()){
      p.checkout(m, LocalDate.now();
    }
  }

  public void returnPublication(Publication p){

    p.pubReturn(LocalDate.now());

  }

  public double getMemberFees(Member m){

    return m.getDueFees();

  }

  public boolean checkMemberFees(Member m){

    return m.getDueFees() != 0;

  }

}
