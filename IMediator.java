package edu.ucsd.cse110.library;

/*
 * HOMEWORK FILE
 */
public interface IMediator {

  public void checkoutPublication(Member m, Publication p);
  public void returnPublication(Publication p);
  public double getMemberFees(Member m);
  public boolean checkMemberFees(Member m)

}
